const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const config = require('../config');
const User = require('../models/user.model');

/**
 * Sign up function
 * Check if the data (email and password) is correct.
 * Then check if the user already exists. If not, hash
 * the password and insert the new user in the database
 */
exports.register = (req, res) => {
  // Handle error 422 BAD JSON
  if (!req.body.email) {
    res.status(422).json({
      error: 'email is required',
    });
  } else if (!req.body.password) {
    res.status(422).json({
      error: 'password is required',
    });
  }

  // Handle error 409 CONFLICT when resource already exists
  User.find({ email: req.body.email })
    .exec()
    .then((result) => {
      if (result.length >= 1) {
        res.status(409).json({
          error: 'email already exists',
        });
      } else {
        // hash the password with salt rounds = 10
        bcrypt.hash(req.body.password, 10)
          .then((hash) => {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash,
            });
            user.save()
              .then(() => {
                res.status(201).json({
                  message: 'User created successfully',
                  user: {
                    email: req.body.email,
                  },
                });
              })
              .catch((err) => {
                res.status(500).json({
                  error: err,
                });
              });
          })
          .catch((err) => {
            res.status(500).json({
              error: err,
            });
          });
      }
    });
};

/**
 * Sign in function
 * Check if the data (email and password) is correct.
 * Then find the user in the database and return his
 * signed Json web token.
 */
exports.login = (req, res) => {
  // Handle error 422 BAD JSON
  if (!req.body.email) {
    res.status(422).json({
      error: 'email is required',
    });
  } else if (!req.body.password) {
    res.status(422).json({
      error: 'password is required',
    });
  }

  User.findOne({ email: req.body.email })
    .exec()
    .then((user) => {
      bcrypt.compare(req.body.password, user.password)
        .then(() => {
          const token = jwt.sign(
            {
              email: user.email,
              userId: user._id,
            },
            config.SECRET,
            {
              expiresIn: '24h',
            },
          );
          res.status(200).json({
            access_token: token,
          });
        })
        .catch(() => {
          res.status(401).json({
            error: 'invalid password',
            user: user.password,
            pass: req.body.password,
          });
        });
    })
    .catch(() => {
      res.status(404).json({
        error: 'User not found',
      });
    });
};

/**
 * Remove user function
 * Delete the first document user that matches conditions from the collection.
 */
exports.delete = (req, res) => {
  User.deleteOne({ _id: req.params.userId })
    .exec()
    .then(() => {
      res.status(200).json({
        message: 'User deleted successfully',
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};
