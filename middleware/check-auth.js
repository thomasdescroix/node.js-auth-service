const jwt = require('jsonwebtoken');
const config = require('../config');

/**
 * Verify json web token
 */
module.exports = (req, res, next) => {
  // Handle token presented as a Bearer token in the Authorization header
  if (req.headers.authorization && req.headers.authorization.split(' '[0] === 'Bearer')) {
    const token = req.headers.authorization.split(' ')[1];
    try {
      const decoded = jwt.verify(token, config.SECRET);
      req.userData = decoded;
      next();
    } catch (err) {
      res.status(401).json({
        error: err.message,
      });
    }
  } else {
    res.status(401).json({
      error: 'No token provided',
    });
  }
};
