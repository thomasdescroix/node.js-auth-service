const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const db = require('./db');
const authRouter = require('./routes/auth.route');

/**
 * Wrong endpoint
 */
const error404 = (req, res, next) => {
  const err = new Error('Not found');
  err.status = 404;
  next(err);
};

/**
 * A "catch-all" function for error handling
 */
const errorHandler = (err, req, res, next) => {
  res.status(err.status || 500).json({
    error: {
      message: err.message,
    },
  });
};

const cors = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'POST, DELETE');
    res.status(200).json({});
  }
  next();
};

const app = express()
  .use(logger('dev'))
  .use(express.json())
  .use(cors)
  .use(express.urlencoded({ extended: false }))
  .use(cookieParser())
  .use('/auth', authRouter)
  .use(error404)
  .use(errorHandler);

module.exports = app;
