# Git conventions

## Format of the commit message

```
<type or gitmoji>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read.

### Type
* `fix`: fixing a bug, critical hotfix, fixing security issues
* `feat`: introducing new features
* `docs`: writing docs
* `style`: improving structure, format of the code or updating the UI and style files
* `refactor`: refactoring code
* `perf`: Improving performance
* `test`: adding tests
* `clean`: removing code or files

### Scope
Scope could be anything specifying place of the commit change.

### Subject
The subject contains succinct description of the change:
* use the imperative ("change" instead of "changes" or "changes")
* don't capitalize first letter
* no dot (.) at the end

### Body
* just as in <subject> use imperative, present tense: “change” not “changed” nor “changes”
* includes motivation for the change and contrasts with previous behavior

### Footer
The footer is the place to reference GitLab Issues that this commit Closes.

### Examples
```
feat(socket): Add the socket management

Implements the socket in the Client and
in the server to send and receive events

Closes #24
```

## gitmoji
You can add emojis on commit messages to identify the purpose or intention of a commit whith only looking at the emojis used.

* `:sparkles:`: introducing new features
* `:bug:`: fixing a bug
* `:ambulance:`: critical hotfix
* `:lock:`: fixing security issues
* `:memo:`: wirting docs
* `:lipstick:`: updating the UI and style files
* `:art:`: improving structure / format of the code
* `:fire:`: removing code or files
* `:zap:`: improving performance
* `:white_check_mark:`: adding tests
* `:construction:`: work in progress
* `:recycle:`: refactoring code
* `:hankey:`: writing bad code that needs to be improved
* `:truck:`: moving or renaming files
* `:beers:`: wirting code drunkenly
* `:building_construction:`: making architectural changes
* `:iphone:`: working on responsive design
