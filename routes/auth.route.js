const express = require('express');
const AuthController = require('../controllers/auth.controller');
const checkAuth = require('../middleware/check-auth');

// TODO: const express = require('express').Router(); directly
const router = express.Router();

// Handle incoming POST requests to /auth/register
router.post('/register', AuthController.register);

// Handle incoming POST requests to /auth/login
router.post('/login', AuthController.login);

// Handle incoming DELETE requests to /auth/delete to test the JWT validation
router.delete('/delete/:userId', checkAuth, AuthController.delete);

module.exports = router;
