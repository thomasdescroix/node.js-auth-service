const mongoose = require('mongoose');

const config = require('./config');

class Database {
  constructor() {
    this.connect();
  }

  /**
   * Set up mongoose connection
   */
  connect() {
    mongoose.connect(config.DATABASE, { useNewUrlParser: true, useCreateIndex: true })
      .then(() => {
        console.log('Database connection successful');
      })
      .catch((err) => {
        console.error(`Database connection error: ${err}`);
        process.exit(1);
      });
  }
}

module.exports = new Database(); // return an instance of the class to implement singleton
