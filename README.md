<img src="https://nodejs.org/static/images/logo-light.svg" alt="Node.js logo" width="200" />

# Auth Service
Micro-service for authentication using Node.js and JWT.

## JWT (JSON Web Token)

see [JWT](https://jwt.io/introduction)
> JWT is an open standard that defines a compact and self-contained way for 
securely transmitting information between parties as a JSON object. This 
information can be verified and trusted because it is digitally signed. [...]

> In authentication, when the user successfully logs in using their credentials, 
a JSON Web Token will be returned. Whenever the user wants to access a protected 
route on resource, the user agent should send the JWT in the Authorization 
header using the Bearer schema. The content of the header should look like the 
following:

```
Authorization: Bearer <token>
```

## Built With
- Node.js, a JavaScript run-time environment
- Express, a web application microframework for Node.js
- MongoDB, a document-oriented database
- Mongoose, a MongoDB object modeling tool

## Setup

Install MongoDB server from
[MongoDB Download Center](https://www.mongodb.com/download-center/community)  

Then clone the repo and run the following command inside the project folder to install the dependencies:
```bash
npm install
```

Edit your MongoDB URL in config.js file:
```JavaScript
module.exports = {
  SECRET: 'YOUR_SECRET_KEY',
  DATABASE: 'YOUR_MONGODB_URL', 
};
```
Now you can use the following scripts.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app using node. 

### `npm dev`

Runs the app in the development mode using nodemon. Note that the application 
will automatically restart you make edits. 

## Endpoints

METHOD | URL | REQUEST_BODY | RESPONSE_SUCCESS | STATUS_CODE
--- | --- | --- | --- | ---
POST | /auth/register | {"email": "test@test.test", "password": "test"} | {"message": "User created successfully", "user": "test@test.test"} | 201 (CREATED)<br />409 (CONFLICT)<br />422 (UNPROCESSABLE ENTITY)<br />500 (INTERNAL SERVER ERROR)
POST | /auth/login | {"email": "test@test.test", "password": "test"} | {"access_token": "TOKEN"} | 200 (OK)<br />400 (NOT JSON)<br />401 (INVALID CREDENTIALS)<br />404 (USER NOT FOUND)<br />422 (UNPROCESSABLE ENTITY)<br />500 (INTERNAL SERVER ERROR)
DELETE | /auth/delete | {"userId": "USER_ID"} | {"message": "User deleted successfully"} | 200 (OK)<br />401 (UNAUTHORIZED)<br />500 (INTERNAL SERVER ERROR)

**Note:** the last endpoint access is protected with JWT. 
